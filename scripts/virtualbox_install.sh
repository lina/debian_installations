#!/bin/bash

# INSTALL VIRTUALBOX FOR DEBIAN 12
sudo apt update -y
sudo wget -P ~/Téléchargements/ https://download.virtualbox.org/virtualbox/6.1.48/virtualbox-6.1_6.1.48-159471~Debian~bookworm_amd64.deb
sudo dpkg -i ~/Téléchargements/virtualbox-6.1_6.1.48-159471~Debian~bookworm_amd64.deb
rm -rf ~/Téléchargements/virtualbox-6.1_6.1.48-159471~Debian~bookworm_amd64.deb

